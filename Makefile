CFLAGS = -I include
#LDFLAGS = -L lib -L /usr/lib/x86_64-linux-gnu
LDFLAGS = -L lib
BUILD = build
LIB = lib 
VPATH = src include

all: clean build_msg ${BUILD}/losh

#${BUILD}/losh: mloop.c
#	${CC} ${CFLAGS} ${LDFLAGS} -o $@ src/mloop.c
${BUILD}/losh: ${LIB}/libbltin.so ${BUILD}/lsh_parse.o mloop.c
	${CC} ${CFLAGS} ${LDFLAGS} -lbltin -lreadline -o $@ ${BUILD}/lsh_parse.o src/mloop.c

${LIB}/libbltin.so: ${BUILD}/bltin.o
	${CC} -shared -o lib/libbltin.so $^

${BUILD}/bltin.o: lib/bltin.c
	${CC} -Wall -I include -fPIC -c $^
	@mv bltin.o ${BUILD}/

${BUILD}/lsh_parse.o: lsh_parse.c
	${CC} -Wall -I include -c $^
	@mv lsh_parse.o ${BUILD}/

.PHONY: clean build_msg check

clean:
	@rm -rf losh losh.o build lib

build_msg:
	@mkdir ${BUILD} ${LIB}
	@printf "\n# Build initiated by $(USER) \n\n"

check:
	@valgrind --leak-check=yes ./build/losh
