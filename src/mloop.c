#include "core.h"
#include<unistd.h>
#include<sys/types.h>

int main(){
    int status;
    char *buf=NULL;
    char *cmds[32];
    size_t len=0;
    extern char **environ;
    pid_t chld;
    //printf("%s",PROMPT);
    //while(getline(&buf,&len,stdin)!=EOF){
    while((buf=readline(PROMPT))!=NULL){
        //printf("%s",buf);
        if(strcmp(buf,"\0")==0){
            continue;
        }
        lsh_parse(buf,cmds);
        //else if(strcmp(cmds[0],"cd")==0){
        if(strcmp(cmds[0],"cd")==0){
            chdir(cmds[1]);
        }
        else{
            chld = fork();
            if(chld == 0){
                exec_c(cmds,environ);
            }
        }
        waitpid(-1,&status,0);
        //printf("Child exited with status: %d\n",status);
        free(buf);
    }
    //free(buf);
    return(0);
}
void exec_c(char **cmds,char **e){
    //if(execvpe(cmds[0],cmds,e)==-1){
    if(execvp(cmds[0],cmds)==-1){
        //perror("execvpe():");
        perror("");
        exit(-1);
    }
    exit(0);
}
