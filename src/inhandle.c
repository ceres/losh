#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<errno.h>

int main(){
    pid_t chld;
    char *line = NULL;
    size_t len = 0;
    //char *arv[4] = {"/bin/ls","-l","/tmp",NULL};
    getline(&line,&len,stdin);
    chld = fork();
    extern char **environ;
    if(chld == 0){
        int retv,stln;
        char *p,*strdp,*tmpr;
        char *cmds[32];
        int counter = 0;
        strdp = strdup(line);
        p = strtok(strdp," ");
        while(p!=NULL){
            stln = strlen(p);
            cmds[counter] = malloc(stln*sizeof(char));
            strncpy(cmds[counter],p,stln);
            //printf("%s\n",cmds[counter]);
            counter++;
            p = strtok(NULL," ");
        }
        //printf("%s\n",cmds[0]);
        //counter++;
        //tmpr = strtok(cmds[--counter],"\n");
        strtok(cmds[--counter],"\n");
        counter++;
        cmds[counter]=malloc(sizeof(NULL));
        cmds[counter]=NULL;
        //for(retv=0;retv<3;retv++){
        //  printf("%s %d\n",cmds[retv],strlen(cmds[retv]));
        //}
        //char *arv[4] = {"/bin/ls","-l","/tmp",NULL};
        //execv(cmds[0],arv);
        execvpe(cmds[0],cmds,environ);
        //if(execv(arv[0],arv)==-1){
        //if(execv(cmds[0],cmds)==-1){
        //   perror("ohno");
        //}
        return(0);
    }
    //printf("Spawned %d\n", chld);
    waitpid(0,NULL,0);
    return(0);


    }
